defmodule Orally.TiledMap do
  alias Orally.TiledMap.Layer

  def load_json(filename) do
    {:ok, file_data} = File.read(filename)
    parse_json(file_data)
  end

  def parse_json(json_str) do
    {:ok, map} = Jason.decode(json_str)

    map
    |> Map.get("layers")
    |> Enum.map(&Map.get(&1, "data"))
    # Drop background layer
    |> Enum.drop(1)
    |> List.zip()
  end

  def dump_json(map) do
    base = Orally.TiledMap.Base.base()
    floor = map |> Enum.map(fn {floor, _objects, _walls} -> floor end)
    objects = map |> Enum.map(fn {_floor, objects, _walls} -> objects end)
    walls = map |> Enum.map(fn {_floor, _objects, walls} -> walls end)

    Map.put(base, "layers", [
      Layer.background_layer(),
      Layer.make_layer(2, "floor", floor),
      Layer.make_layer(3, "objects", objects),
      Layer.make_layer(4, "walls", walls)
    ])
    |> Jason.encode()
  end

  def save_json(json_map, filename) do
    :ok = File.write(filename, json_map)
  end

  def honk() do
    map = load_json("test.json")
    shuffled = Enum.shuffle(map)
    {:ok, dumped} = dump_json(shuffled)
    save_json(dumped, "out.json")
  end
end
