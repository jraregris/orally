defmodule Orally.Marko do
  defmodule Trigrams do
    def add(trigrams, trigram) when is_list(trigram) do
      {_, map} =
        Map.get_and_update(trigrams, trigram, fn current_value ->
          if current_value == nil do
            {current_value, 1}
          else
            {current_value, current_value + 1}
          end
        end)

      map
    end

    def print(trigrams) when is_map(trigrams) do
      for {trigram, n} <- trigrams do
        trigram =
          Enum.map(trigram, fn letter ->
            case letter do
              :start -> "^"
              :stop -> "$"
              _ -> letter
            end
          end)

        IO.puts("#{trigram}: #{n}")
      end
    end
  end

  def eat(data) when is_binary(data) do
    data |> String.codepoints() |> eat()
  end

  def eat(data) when is_list(data) do
    chew([:start, :start] ++ data, %{})
  end

  def chew([], trigrams) do
    trigrams
  end

  def chew(data, trigrams) when is_list(data) and is_map(trigrams) do
    case Enum.split(data, 3) do
      {[uno], []} ->
        chew([], Trigrams.add(trigrams, [uno, :stop, :stop]))

      {[uno, dos], []} ->
        chew([dos], Trigrams.add(trigrams, [uno, dos, :stop]))

      {[uno, dos, tres], rest} ->
        chew([dos, tres] ++ rest, Trigrams.add(trigrams, [uno, dos, tres]))
    end
  end
end
