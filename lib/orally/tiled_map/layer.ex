defmodule Orally.TiledMap.Layer do
  def background_layer() do
    %{
      "data" => [1, 2, 3, 4, 5, 6, 1, 2, 3, 4, 7, 8, 9, 10, 11, 12, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 1, 2, 3, 4, 7, 8, 9, 10, 11, 12, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 1, 2, 3, 4, 7, 8, 9, 10, 11, 12, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 1, 2, 3, 4, 7, 8, 9, 10, 11, 12, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 1, 2, 3, 4, 7, 8, 9, 10, 11, 12, 7, 8, 9, 10],
      "height" => 10,
      "id" => 1,
      "name" => "background",
      "opacity" => 1,
      "type" => "tilelayer",
      "visible" => true,
      "width" => 10,
      "x" => 0,
      "y" => 0
     }
  end

  def make_layer(id, name, data) do
    %{
      "data" => data,
      "height" => 10,
      "id" => id,
      "name" => name,
      "opacity" => 1,
      "type" => "tilelayer",
      "visible" => true,
      "width" => 10,
      "x" => 0,
      "y" => 0
    }
  end
end
